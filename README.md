# Airbnb Amsterdam - data analysis

## Description
Answering some questions for municipality, house owners and tourists regarding some Airbnb data from July 2016. This was an assignment for the company i2i. Especially concerning the housing problems of Amsterdam caused by Airbnb.

<!-- ## Result -->


## Files and usage
Below is a list of the files. The case description is the original assignment from the company i2i. There is a jupyter notebook file containing the code and analysis. There is also the data/csv file from Airbnb. In this file the column availability_365 indicates the number of days in the next 365, when the listing is available (so also not booked).

1. README.md
2. i2i - Case Description.txt
3. Listings.csv
4. Airbnb.ipynb

## Contact
Victor Boogers  
VBgetsmail@gmail.com
